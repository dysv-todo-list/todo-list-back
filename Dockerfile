FROM node:15.14.0-alpine3.11

WORKDIR /app

COPY . .

RUN yarn install

CMD ["node","index.js"]