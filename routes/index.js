const { Router } = require('express');
const router = Router();

const { getTodos, getTodoById, createTodo, updateTodo, deleteTodo} = require('../controllers/index.controller');

router.get('/todos', getTodos);
router.get('/todos/:id', getTodoById);
router.post('/todos', createTodo);
router.put('/todos/:id', updateTodo)
router.delete('/todos/:id', deleteTodo);

module.exports = router;