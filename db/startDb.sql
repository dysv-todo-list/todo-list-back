CREATE DATABASE todosdb;

CREATE TABLE todos(
    id SERIAL PRIMARY KEY,
    title VARCHAR(40) NOT NULL,
    is_completed BOOLEAN NOT NULL DEFAULT FALSE
);

INSERT INTO todos (title, is_completed)
    VALUES ('todo 1', TRUE),
    ('todo 2', FALSE);

select * from todos;