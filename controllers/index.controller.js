const dbconfig = require('../db/db.config')
const { Pool } = require('pg');

const pool = new Pool(dbconfig);

const getTodos= async (req, res) => {
    const response = await pool.query('SELECT * FROM todos ORDER BY id ASC');
    res.status(200).json(response.rows);
};

const getTodoById = async (req, res) => {
    const id = parseInt(req.params.id);
    const response = await pool.query('SELECT * FROM todos WHERE id = $1', [id]);
    res.json(response.rows);
};

const createTodo = async (req, res) => {
    const { title } = req.body;
    const response = await pool.query('INSERT INTO todos (title) VALUES ($1) RETURNING id, title, is_completed', [title]);
    res.json({
        message: 'todo añadido',
        body: {
            todo: response.rows[0]
        }
    })
};

const updateTodo = async (req, res) => {
    const id = parseInt(req.params.id);
    const { title, is_completed } = req.body;

    const response =await pool.query('UPDATE todos SET title = $1, is_completed = $2 WHERE id = $3', [
        title,
        is_completed,
        id
    ]);
    res.json('ToDo Actualizado');
};

const deleteTodo = async (req, res) => {
    const id = parseInt(req.params.id);
    await pool.query('DELETE FROM todos where id = $1', [
        id
    ]);
    res.json(`ToDo ${id} borrado`);
};

module.exports = {
    getTodos,
    getTodoById,
    createTodo,
    updateTodo,
    deleteTodo
};